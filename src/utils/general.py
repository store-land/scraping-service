import re


def remove_space(val: str, space: str = " ", default: str = "") -> str:
    val = val or default
    return space.join(val.split())


def clean_str(val: str, default: str = "") -> str:
    return remove_space(val, default=default)


def clean_int(val: str, default: int = 0) -> int:
    if val is None:
        return default
    val = remove_space(val)
    val = re.sub("[^0-9]", "", val)
    if len(val) == 0:
        return default
    return int(val)


def clean_float(val: str, default: float = 0.0) -> float:
    if val is None:
        return default
    val = remove_space(val)
    val = re.sub("[^0-9\.]", "", val)
    if len(val) == 0:
        return default
    return float(val)
