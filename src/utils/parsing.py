from typing import Optional, Union

from bs4 import BeautifulSoup

from src.utils.general import clean_float, clean_int, clean_str


def get_safe_data(
    item: BeautifulSoup,
    get: str = "inner",
    default: Optional[Union[str, int, float]] = "",
) -> Optional[Union[str, int, float]]:
    if item:
        if get == "inner":
            return item.get_text()
        else:
            return item.get(get, default)
    return default


def get_safe_item(
    soup: BeautifulSoup,
    css_select: str,
    get: str = "inner",
    default: Optional[Union[str, int, float]] = "",
    type: str = "str",
) -> Optional[Union[str, int, float]]:
    temp = soup.select_one(css_select)
    value = get_safe_data(temp, get, default)

    if value is None:
        return None

    if type == "int":
        return clean_int(value)
    elif type == "float":
        return clean_float(value)
    else:
        return clean_str(value)
