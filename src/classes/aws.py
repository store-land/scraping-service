import json
import logging
from typing import Dict, Optional

import boto3

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


class AWS:
    def __init__(self) -> None:
        pass

    def get_client(self, service: str) -> Optional[any]:
        try:
            return boto3.client(service)
        except Exception as e:
            LOGGER.info(e)
            return None

    def exec_lambda(
        self,
        lambda_name: str,
        payload: Dict = {},
        async_exec: bool = True,
    ) -> bool:
        client = self.get_client("lambda")
        if not client:
            return False

        invocation_type = "Event" if async_exec else "RequestResponse"
        try:
            resp = client.invoke(
                FunctionName=lambda_name,
                Payload=json.dumps(payload),
                InvocationType=invocation_type,
            )
            status_code = resp["StatusCode"]
            if async_exec:
                return status_code == 202
            else:
                return status_code == 200
        except Exception as e:
            LOGGER.info(e)
            return False
