from src.classes.scraping.cyberpuerta import Cyberpuerta
from src.classes.scraping.ddtech import DDTech

__all__ = ["Cyberpuerta", "DDTech"]
