import logging
from typing import Dict, List, Optional

from slugify import slugify

from src.classes.scraping.store import Store
from src.utils.parsing import get_safe_item

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


class DDTech(Store):
    def __init__(self) -> None:
        Store.__init__(self, "DDTech")

    def scrap_category(
        self, url: str, results: Optional[List[Dict]] = None
    ) -> List[Dict]:
        LOGGER.info(
            f"{self.store_name} - {self.current_pagination + 1}/{self.max_pagination} - {url}"
        )
        resp = self.http_client.get(url)
        soup = self.soupify_response(resp)

        if soup is None:
            return []

        if results is None:
            results = []

        html_products = soup.select(".row > .item")
        for row in html_products:
            title = get_safe_item(row, "a", get="title")
            url = get_safe_item(row, "a", get="href")
            store_sku = url.split("=")[-1]
            slug = slugify(f"{title}-{store_sku}")
            image_url = get_safe_item(row, ".product-image img", get="data-echo")

            price = get_safe_item(row, ".price", type="float", default=0.0)
            price_old = get_safe_item(
                row, ".price-before-discount", type="float", default=0.0
            )
            has_stock = get_safe_item(row, ".with-stock")
            has_stock = has_stock == "CON EXISTENCIA"

            results.append(
                {
                    "product": {
                        "storeSku": store_sku,
                        "title": title,
                        "url": url,
                        "slug": slug,
                        "imageUrl": image_url,
                    },
                    "productHistory": {
                        "price": price,
                        "priceOld": price_old,
                        "stockQty": None,
                        "hasStock": has_stock,
                    },
                }
            )

        next_url = get_safe_item(soup, 'a[rel="next"]', get="href")
        self.current_pagination += 1
        if next_url and self.current_pagination < self.max_pagination:
            return self.scrap_category(next_url, results)

        self.current_pagination = 0
        return results
