import logging
from typing import Dict, List, Optional

from slugify import slugify

from src.classes.scraping.store import Store
from src.utils.parsing import get_safe_item

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


class Cyberpuerta(Store):
    def __init__(self) -> None:
        Store.__init__(self, "Cyberpuerta")

    def scrap_category(
        self, url: str, results: Optional[List[Dict]] = None
    ) -> List[Dict]:
        LOGGER.info(
            f"{self.store_name} - {self.current_pagination + 1}/{self.max_pagination} - {url}"
        )
        resp = self.http_client.get(url)
        soup = self.soupify_response(resp)

        if soup is None:
            return []

        if results is None:
            results = []

        html_products = soup.select(".productData")
        for row in html_products:
            store_sku = get_safe_item(row, ".emproduct_right_artnum")
            store_sku = store_sku.replace("Art: ", "")
            store_sku = "-".join(store_sku.split()).upper()

            title = get_safe_item(row, ".emproduct_right_title")
            url = get_safe_item(row, ".emproduct_right_title", get="href")
            slug = slugify(f"{title}-{store_sku}")

            image_url = get_safe_item(row, ".catSlider", get="data-cp-prod-slider")
            image_url = image_url.replace("&quot;", "")
            image_url = image_url.replace("\/", "/")
            image_url = image_url[1:-1]
            image_url = image_url.split(",")
            image_url = image_url[0]
            image_url = image_url.replace("/S/", "/L/")
            image_url = image_url.replace('"', "")

            price = get_safe_item(row, ".price", type="float", default=0.0)
            price_old = get_safe_item(row, ".oldPrice", type="float", default=0.0)
            stock_qty = get_safe_item(row, ".emstock > span", type="int", default=None)
            if stock_qty is None:
                has_stock = None
            else:
                has_stock = stock_qty > 0

            results.append(
                {
                    "product": {
                        "storeSku": store_sku,
                        "title": title,
                        "url": url,
                        "slug": slug,
                        "imageUrl": image_url,
                    },
                    "productHistory": {
                        "price": price,
                        "priceOld": price_old,
                        "stockQty": stock_qty,
                        "hasStock": has_stock,
                    },
                }
            )

        next_url = get_safe_item(soup, ".next", get="href")
        self.current_pagination += 1
        if next_url and self.current_pagination < self.max_pagination:
            return self.scrap_category(next_url, results)

        self.current_pagination = 0
        return results
