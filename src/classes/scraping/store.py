import logging
import os
from typing import Optional

import requests
from bs4 import BeautifulSoup

from src.classes.http_client import HttpClient

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)

max_pagination = os.getenv("MAX_PAGINATION")


class Store:
    def __init__(self, store_name: str, url: str = "") -> None:
        self.http_client = HttpClient()
        self.store_name = store_name
        self.base_url = url
        self.max_pagination = int(max_pagination)
        self.current_pagination = 0

    def soupify_response(
        self,
        resp: Optional[requests.models.Response],
    ) -> Optional[BeautifulSoup]:
        if resp is None:
            return None

        if resp.status_code < 200 or resp.status_code > 299:
            LOGGER.info(resp.text)
            return None

        return BeautifulSoup(resp.text, "lxml")
