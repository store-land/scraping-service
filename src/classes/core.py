import logging
import os
import time
from functools import wraps
from typing import Dict, List, Optional, Union

import requests

from src.classes.http_client import HttpClient
from src.utils.constants import CORE_API_URL, LAMBDA_NAME

ENV = os.getenv("ENV")


LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


def func_log(f):
    @wraps(f)
    def decorate(*args, **kwargs):
        LOGGER.info(f"[{f.__name__}] Requesting...")
        data = f(*args, **kwargs)
        LOGGER.info(f"[{f.__name__}] Success: {data is not None}")
        return data

    return decorate


def retry_helper(f):
    @wraps(f)
    def decorate(*args, **kwargs):
        LOGGER.info(f"[{f.__name__}] Retry...")

        max_retry = 3
        tries = 0
        while tries <= max_retry:
            data = f(*args, **kwargs)
            if data is not None:
                break
            tries += 1
            time.sleep(3)

        LOGGER.info(f"[{f.__name__}] Success: {data is not None}")

        return data

    return decorate


def format_response(
    resp: Optional[requests.models.Response],
) -> Optional[Union[Dict, List]]:
    if resp is None:
        return None

    if resp.status_code == 200:
        return resp.json()
    else:
        LOGGER.info(resp.text)
        return None


class Core:
    def __init__(self) -> None:
        self.API_URL = CORE_API_URL[ENV]
        self.http_client = HttpClient()
        self.http_client.session.headers["user-agent"] = f"{LAMBDA_NAME}-{ENV}"

    @func_log
    def login(self, email: str, password: str) -> bool:
        resp = self.http_client.post(
            f"{self.API_URL}/api/auth/login",
            json={"email": email, "password": password},
        )
        data = format_response(resp)
        if data is None:
            return False

        self.http_client.session.headers["authorization"] = f'Bearer {data["token"]}'
        return True

    @retry_helper
    def post_scraping_product(self, payload: Dict) -> Optional[Dict]:
        return format_response(
            self.http_client.post(
                f"{self.API_URL}/api/scraping/product",
                json=payload,
            )
        )

    @retry_helper
    def post_store_category_map_product(
        self, store_id: int, category_map_id: int, product: Dict
    ) -> Optional[bool]:
        resp = self.http_client.post(
            f"{self.API_URL}/api/store/{store_id}/category-map/{category_map_id}/product",
            json=product,
        )

        data = format_response(resp)
        if data is None:
            return None
        return True
