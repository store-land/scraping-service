import logging
import os
from typing import Dict, List

from src.classes import AWS, Core
from src.classes.scraping import Cyberpuerta, DDTech
from src.utils.constants import LAMBDA_EXEC
from src.utils.helpers import chunks

LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)


def main(event: Dict, context) -> Dict:
    store_id: int = event.get("storeId", 0)
    store_slug: str = event.get("storeSlug", 0)
    categories: List[Dict] = event.get("categories", [])
    current_idx: int = event.get("currentIdx", 0)
    max_idx: int = event.get("maxIdx", 0)

    if not store_id:
        return {"statusCode": 400, "message": "Invalid store id"}
    if not store_slug:
        return {"statusCode": 400, "message": "Invalid store slug"}
    if not categories:
        return {"statusCode": 400, "message": "Invalid categories"}
    if not max_idx:
        return {"statusCode": 400, "message": "Invalid max index"}

    core = Core()
    email = os.getenv("CORE_EMAIL")
    password = os.getenv("CORE_PASSWORD")
    core.login(email, password)

    cyberpuerta = Cyberpuerta()
    ddtech = DDTech()

    store_scraper = None
    if store_slug == "ddtech":
        store_scraper = ddtech
    elif store_slug == "cyberpuerta":
        store_scraper = cyberpuerta
    else:
        return {"statusCode": 400, "message": "Invalid slug"}

    aws = AWS()

    category = categories[current_idx]
    products = store_scraper.scrap_category(category["url"])

    chunked_products = list(chunks(products, 30))
    status = {"success": 0, "errors": 0}
    for part in chunked_products:
        products_ids = core.post_scraping_product(part)
        if products_ids:
            success = core.post_store_category_map_product(
                store_id, category["id"], products_ids
            )
            if success:
                status["success"] += 1
            else:
                status["errors"] += 1
        else:
            LOGGER.info("Can't POST the products :c")

    next_idx = current_idx + 1
    if next_idx == max_idx:
        LOGGER.info("Wuhuu, last lambda c:")
        return status

    resp = aws.exec_lambda(
        LAMBDA_EXEC,
        payload={
            "storeId": store_id,
            "storeSlug": store_slug,
            "categories": categories,
            "currentIdx": next_idx,
            "maxIdx": max_idx,
        },
    )

    status["callLambdaSuccess"] = resp
    return status
